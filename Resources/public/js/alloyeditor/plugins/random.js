(function(global) {
    if (CKEDITOR.plugins.get('random')) {
        return;
    }

    const InsertRandomNumber = {
        exec: function(editor) {
            const number = Math.floor(Math.random() * 1000) + 100;
            editor.focus();
            editor.insertText(' ' + number);
        },
    };

    /**
     * CKEditor plugin providing the InsertRandomNumber command. This command
     * generates and inserts new random number
     *
     * @class ezremoveblock
     * @namespace CKEDITOR.plugins
     * @constructor
     */
    CKEDITOR.plugins.add('random', {
        init: (editor) => editor.addCommand('InsertRandomNumber', InsertRandomNumber),
    });
})(window);

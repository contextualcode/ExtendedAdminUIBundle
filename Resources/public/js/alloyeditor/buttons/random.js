import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AlloyEditor from 'alloyeditor';
import EzButton from '../../../../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/base/ez-button.js';

export default class BtnRandom extends EzButton {
    static get key() {
        return 'random';
    }

    /**
     * Executes the InsertRandomNumber to insert random number.
     *
     * @method random
     * @protected
     */
    insertRandomNumber(data) {
        this.execCommand(data);
    }

    /**
     * Lifecycle. Renders the UI of the button.
     *
     * @method render
     * @return {Object} The content which should be rendered.
     */
    render() {
        const title = 'Random';

        return (
            <button
                className="ae-button ez-btn-ae ez-btn-ae--random"
                onClick={this.insertRandomNumber.bind(this)}
                tabIndex={this.props.tabIndex}
                title={title}>
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/extendedadminui/img/random-icon.svg#random" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnRandom.key] = AlloyEditor.BtnRandom = BtnRandom;
eZ.addConfig('ezAlloyEditor.BtnRandom', BtnRandom);

BtnRandom.propTypes = {
    command: PropTypes.string,
};

BtnRandom.defaultProps = {
    command: 'InsertRandomNumber',
};
